const jwt = require('jsonwebtoken');
require('dotenv').config();
const User = require('../models/User'); // import your user model

const verifyEmail = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  if (!authHeader) return res.sendStatus(401);
  const token = authHeader.split(' ')[1];
  jwt.verify(
    token,
    process.env.ACCESS_TOKEN_SECRET,
    async (err, decoded) => {
      if (err) return res.sendStatus(403); //invalid token
      req.userId = decoded.id;
      const user = await User.findById(req.userId);
      if (!user) return res.sendStatus(404); //user not found
      // Check if the emailVerifiedAt field is not null
      if (!user.emailVerifiedAt) return res.sendStatus(403); //email not verified
      next();
    }
  );
}

module.exports = verifyEmail;
