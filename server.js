const mongo = require('./db.config');
const express = require('express');
const authController = require('./controllers/authController');
const authValidations = require('./validations/authValidations');
const urlController = require('./controllers/urlController');
const urlValidations = require('./validations/urlValidations');
const userController = require('./controllers/userController');
const cookieParser = require('cookie-parser');
const auth = require('./middleware/Auth');
const verified = require('./middleware/Verified');
const app = express();
app.use(express.json());
app.use(cookieParser());


app.post('/auth/register',authValidations.create ,authController.create );
app.get('/auth/verify-email/:tokenId', authValidations.verifyEmail, authController.verifyEmail);
app.post('/auth/resend-verification-email', authController.resendVerificationEmail);

app.use(['/profile', '/urls', '/edit'], auth);
app.use(['/profile', '/urls', '/edit', '/auth/logout', '/auth/refresh'], verified);
app.post('/auth/logout', authController.destroy);
app.use('/auth/refresh', authController.refresh);
app.post('/auth/login',authValidations.store, authController.store);




//get urls of logged in user
app.get('/profile/urls', userController.get);
//redirect by short url tag
app.get('/:shortUrlTag([a-zA-Z0-9]+)', urlValidations.get, urlController.get);
app.post('/urls',urlValidations.create, urlController.create);
//get original url without redirecting to it
app.get('/urls/:shortUrlId([a-zA-Z0-9-]+)',urlValidations.show, urlController.show);
app.delete('/urls/:shortUrlId([a-zA-Z0-9-]+)',urlValidations.destroy, urlController.destroy);
app.put('/urls/:shortUrlId([a-zA-Z0-9-]+)',urlValidations.update, urlController.update);

module.exports = {app};