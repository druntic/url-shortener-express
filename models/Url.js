const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuid = require('node-uuid');

const urlSchema = new Schema({
    _id: { type: String, default: uuid.v4},
    trueUrl: {
        type: String,
        required: true,   
    },
    shortUrl: {
        type: String,
        required: true,   
    },
    author: {
        type: String,
        required: true,   
    },
    counter: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('url', urlSchema);