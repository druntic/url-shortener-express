const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuid = require('node-uuid');

const tokenSchema = new Schema({
    _id: { type: String, default: uuid.v4},
    entityId: {
        type: String,
        required: true,   
    },
    actionName: {
        type: String,
        required: true,   
    },
    createdAt: {
        type: Date,
        default: Date.now   
    },
    updatedAt: {
        type: Date,
        default: Date.now
         
    },
    executedAt: {
        type: Date,
        default : null
    },
    expiresAt: {
        type: Date,
        default: () => {
          // Add 15 minutes to the current time
          return new Date(Date.now() + 900000);
        }
       }
       

});

module.exports = mongoose.model('token_action', tokenSchema);