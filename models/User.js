const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uuid = require('node-uuid');

const userSchema = new Schema({
    _id: { type: String, default: uuid.v4},
    username: {
        type: String,
        required: true,   
    },
    email: {
        type: String,
        required: true,   
    },
    password: {
        type: String,
        required: true,   
    },
    refreshToken: {
        type: String
    },
    emailVerifiedAt: {
        type: Date,
        default : null
    },
});

module.exports = mongoose.model('User', userSchema);