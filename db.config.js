const mongoose = require('mongoose');
require('dotenv').config();
mongoose.connect(process.env.DB_URL, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.once('open', function() { console.log('Connected to MongoDB!'); });

module.exports = mongoose;