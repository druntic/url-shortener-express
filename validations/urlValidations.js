const { body, query, param } = require("express-validator"); // import express validator
const Url = require('../models/Url');
const createError = require('http-errors');
const mongoose = require('mongoose');

module.exports = {
    get: [
    ],
    create: [
        body('shortUrl').custom(async (shortUrl, { req }) => {
            // Define an array of forbidden shortUrl names
            const forbiddenNames = ["login", "register", "create", "profile", "update", "delete", "read"];
            // Check if shortUrl is in the forbiddenNames array
            if (forbiddenNames.includes(shortUrl)) {
                // If it is, throw an error
                throw createError(403,"ShortUrl name is forbidden");
            }
            // Get the protocol and host from the request
            if (!shortUrl) {
                shortUrl = '';
            }

            // If the url is found, return true
            return true;
        }),
    ],
    update: [
        param('shortUrlId').isUUID().notEmpty().withMessage('invalid short url id').custom(async (shortUrlId, { req }) => {


            // Define an array of forbidden shortUrl names
            var shortUrl = req.body.shortUrl;
            const forbiddenNames = ["login", "register", "create", "profile", "update", "delete", "read"];
            // Check if shortUrl is in the forbiddenNames array
            if (forbiddenNames.includes(shortUrl)) {
                // If it is, throw an error
                throw createError(403,"ShortUrl name is forbidden");
            }
            return true;
        })
    ],
    destroy: [
        param('shortUrlId').isUUID().notEmpty().withMessage('invalid short url id').custom(async (shortUrlId, { req }) => {
            const result = await Url.findById(shortUrlId);
            if (!result) throw createError(404, 'url with short url id not found');
        })
    ],
    show: [
        param('shortUrlId').isUUID().notEmpty().withMessage('invalid short url id').custom(async (shortUrlId, { req }) => {
            const result = await Url.findById(shortUrlId);
            if (!result) throw createError(404, 'url with short url id not found');
        })
    ],


}