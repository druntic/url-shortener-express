const { body, query, param } = require("express-validator"); // import express validator
const ActionToken = require('../models/ActionToken');
const createError = require('http-errors');
const mongoose = require('mongoose');

module.exports = {
    create: [
        body("username").notEmpty().withMessage("Username cannot be empty"),
        body("email").isEmail().withMessage("Invalid email address"), // check if email is a valid email address
        body("password")
          .isLength({ min: 6 })
          .withMessage("Password must be at least 6 characters long"), // check if password is at least 6 characters long
      ],

      store: [
        body("email").notEmpty().withMessage("Email cannot be empty"),
        body("password").notEmpty().withMessage("Password cannot be empty")
      ],

      verifyEmail: [
        param("tokenId").isUUID().custom(async (tokenId, {req}) =>{
          const result = await ActionToken.findById(tokenId);
          if (!result) throw createError(500, 'invalid token id');
      })
      ]
}