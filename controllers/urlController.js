const { validationResult } = require('express-validator');
const urlService = require('../services/urlService');
class urlController {

  async create(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      var { url, shortUrl } = req.body;
      // wait for the promise to resolve
      const result = await urlService.create(url, shortUrl, req.userId);
      // send a success response with the created url
      res.status(200).json({ message: 'short url created', result: result });
    } catch (error) {
      // handle any errors
      res.status(error.status).json({ message: error.message });
    }
  }

  async destroy(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const shortUrlId = req.params.shortUrlId;
      // wait for the promise to resolve
      const result = await urlService.destroy(shortUrlId, req.userId);
      // send a success response
      res.status(200).json({ message: 'short url deleted' });
    } catch (error) {
      // handle any errors
      res.status(error.status).json({ message: error.message });
    }
  }


  async get(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const shortUrlTag = req.params.shortUrlTag;
      // wait for the promise to resolve
      const result = await urlService.get(shortUrlTag);
      // send a success response
      res.status(301).redirect(result);
    } catch (error) {
      // handle any errors
      res.status(error.status).json({ message: error.message });
    }
  }


  async show(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const shortUrlId = req.params.shortUrlId;
      // wait for the promise to resolve
      const result = await urlService.show(shortUrlId, req.userId);
      // send a success response 
      res.status(200).json({ message: result.trueUrl });
    } catch (error) {
      // handle any errors
      res.status(error.status).json({ message: error.message });
    }
  }


  async update(req, res) {
    // Get the validation errors from the request
    const errors = validationResult(req);
    // If there are errors, send a 400 response with the error messages
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const { shortUrl, url } = req.body;
      const shortUrlId = req.params.shortUrlId;
      // wait for the promise to resolve
      const result = await urlService.update(shortUrl, url, shortUrlId, req.userId);
      // send a success response 
      res.status(200).json({ message: result });
    } catch (error) {
      // handle any errors
      res.status(error.status).json({ message: error.message });
    }
  }


}

module.exports = new urlController();