const { validationResult } = require('express-validator');
const userService = require('../services/userService');
class userController {

    async get(req, res) {
        // Get the validation errors from the request
        const errors = validationResult(req);
        // If there are errors, send a 400 response with the error messages
        if (!errors.isEmpty()) {
          return res.status(400).json({ errors: errors.array() });
        }
        try {
          const currentPage = req.query.currentPage || 1;
          const perPage = req.query.perPage || 10;
          // wait for the promise to resolve
          const result = await userService.get(req.userId, currentPage, perPage);
          // send a success response with the results
          res.status(200).json({ result });
        } catch (error) {
          // handle any errors
          res.status(error.status).json({ message: error.message, currentPage, perPage });
        }
      }
      
}

module.exports = new userController();