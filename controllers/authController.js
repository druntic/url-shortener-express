const authService = require('../services/authService');
const { validationResult } = require('express-validator');

class authController {
    //login user
    async store(req, res) {
        // Get the validation errors from the request
        const errors = validationResult(req);
        // If there are errors, send a 400 response with the error messages
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        try {
            const { email, password } = req.body;
            // wait for the promise to resolve
            const result = await authService.store(email, password);
            // send a success response with the access token of the logged user
            res.cookie('jwt', result.refreshToken, { httpOnly: false, sameSite: 'None', maxAge: 24 * 60 * 60 * 1000 });
            res.json({ accessToken: result.accessToken });
            //res.status(200).json({message : 'user logged in' });
        } catch (error) {
            // handle any errors
            res.status(error.status).json({ message: error.message });
        }
    }

    //logout user
    async destroy(req, res) {
        // Get the validation errors from the request
        const errors = validationResult(req);
        // If there are errors, send a 400 response with the error messages
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        try {
            const cookies = req.cookies;
            // wait for the promise to resolve
            const result = await authService.destroy(cookies);
            // clear the cookie and send a success response
            res.clearCookie('jwt');
            res.status(200).json({ message: 'user logged out' });
        } catch (error) {
            // handle any errors
            res.status(error.status).json({ message: error.message });
        }
    }

    //create user
    async create(req, res) {
        // Get the validation errors from the request
        const errors = validationResult(req);
        // If there are errors, send a 400 response with the error messages
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        try {
            const { username, email, password } = req.body;
            // wait for the promise to resolve
            const id = await authService.create(username, email, password);
            try {
                await authService.sendVerificationEmail(email, id);
            } catch (err) {
                return res.status(500).json({ message: 'user created in database but verify email link could not be sent due to error in nodemailer config' });
            }
            // send a success response with the created user
            res.status(200).json({ message: 'user created and verification email sent' });
        } catch (error) {
            // handle any errors
            res.status(error.status).json({ message: error.message });
        }
    }

    //refresh token
    async refresh(req, res) {
        // Get the validation errors from the request
        const errors = validationResult(req);
        // If there are errors, send a 400 response with the error messages
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        try {
            const cookies = req.cookies;
            // wait for the promise to resolve
            const result = await authService.refresh(cookies);
            // send a success response with the new access token
            res.json({ accessToken: result });
        } catch (error) {
            // handle any errors
            res.status(error.status).json({ message: error.message });
        }
    }


    //verify email
    async verifyEmail(req, res) {
        // Get the validation errors from the request
        const errors = validationResult(req);
        // If there are errors, send a 400 response with the error messages
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        try {
            const actionTokenId = req.params.tokenId;
            // wait for the promise to resolve
            const result = await authService.verifyEmail(actionTokenId);
            res.cookie('jwt', result.refreshToken, { httpOnly: false, sameSite: 'None', maxAge: 24 * 60 * 60 * 1000 });
            res.json({ accessToken: result.accessToken, message: 'user email verified' });

        } catch (error) {
            // handle any errors
            res.status(error.status).json({ message: error.message });
        }
    }

    // Handle the response in the controller based on the callback result
    async resendVerificationEmail(req, res) {
        // Get the validation errors from the request
        const errors = validationResult(req);
        // If there are errors, send a 400 response with the error messages
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        try {
            await authService.resendVerificationEmail(req.body.email, (error, info) => {
                if (error) {
                    // Handle the error, e.g. log it or send a response
                    
                    res.status(500).json({ message: 'email could not be sent' });
                } else {
                    console.log('Email sent: ' + info.response);
                    res.status(200).json({ message: 'verification email sent' });
                }
            });

        } catch (error) {
            // handle any errors
            res.status(error.status).json({ message: error.message });
        }

    }

}

module.exports = new authController();