
## Summary

URL shortener using Express.js as the backend framework. The application allows users to shorten long URLs into shorter, more manageable links.
Additionally, it incorporates JWT (JSON Web Token) authentication for user registration and login purposes,
enabling users to shorten URLs and see statistics.
