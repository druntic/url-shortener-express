
const express = require('express');
const authController = require('../controllers/authController');
const authValidations = require('../validations/authValidations');
const urlController = require('../controllers/urlController');
const urlValidations = require('../validations/urlValidations');
const userController = require('../controllers/userController');
const cookieParser = require('cookie-parser');
const auth = require('../middleware/Auth');
const app = express();
const PORT = 8080;
app.use(express.json());
app.use(cookieParser());
const server = app.listen(
    PORT,
    () => console.log(`its alive on http://localhost:${PORT}`)
);

app.post('/auth/register',authValidations.create ,authController.create );
app.post('/auth/login',authValidations.store, authController.store);
app.post('/auth/logout', authController.destroy);
app.get('/auth/verify-email/:token_id', authController.verifyEmail);
app.use('/auth/refresh', authController.refresh);



app.use(['/profile', '/urls', '/edit'], auth);

//get urls of logged in user
app.get('/profile/urls', userController.get);
//redirect by short url tag
app.get('/:shortUrlTag([a-zA-Z0-9]+)', urlValidations.get, urlController.get);
app.post('/urls',urlValidations.create, urlController.create);
//get original url without redirecting to it
app.get('/urls/:shortUrlId([a-zA-Z0-9]+)',urlValidations.show, urlController.show);
app.delete('/urls/:shortUrlId([a-zA-Z0-9]+)',urlValidations.destroy, urlController.destroy);
app.put('/urls/:shortUrlId([a-zA-Z0-9]+)',urlValidations.update, urlController.update);

module.exports = {app, server}