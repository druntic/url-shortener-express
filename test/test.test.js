const { MongoMemoryServer } = require('mongodb-memory-server');
const User = require('../models/User');
const request = require('supertest');
const mongoose = require('mongoose');

const { app, server } = require('./server'); // this is the file where you define your app and routes
// Create a new instance of the in-memory Mongo server
const mongoServer = new MongoMemoryServer();
beforeAll(async () => {
    await mongoServer.start();
    // Wait for a short delay to ensure the server is running
    await new Promise((resolve) => setTimeout(resolve, 1000));
    // Get the connection string of the in-memory database
    const mongoUri = mongoServer.getUri();
    // Check if mongoose is already connected
    if (mongoose.connection.readyState === 0) {
        // Connect to the in-memory database
        await mongoose.connect(mongoUri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
    }
});
afterAll(async () => {
    // Disconnect Mongoose and stop the in-memory database
    await mongoose.disconnect();
    await mongoServer.stop();
    await server.close()
});

//auth tests
test('Create a new user', async () => {
    // Create a mock user object
    const mockUser = {
        username: 'hash2',
        email: 'hash2@ketchum.com',
        password: '123456',
    };
    // Make a POST request to create a new user
    const response = await request(app)
        .post('/auth/register')
        .send(mockUser);
    // Assert
    // Expect that the response status is 201 (created)
    expect(response.status).toBe(200);
    // Retrieve the created user from the database using Mongoose
    const createdUser = await User.findOne({ email: mockUser.email });
    // Expect that the user exists in the database
    expect(createdUser).toBeDefined();
    expect(createdUser.username).toBe(mockUser.username);
    expect(createdUser.email).toBe(mockUser.email);
});

test('login as user', async () => {
    // Create a mock user object
    const mockUser = {
        username: 'hash2',
        email: 'hash2@ketchum.com',
        password: '123456',
    };
    // Make a POST request to create a new user
    const response = await request(app)
        .post('/auth/login')
        .send(mockUser);
    // Assert
    // Expect that the response status is 200 (ok)
    expect(response.status).toBe(200);
});

test('logout user', async () => {
    // Create a mock user object
    const mockUser = {
        username: 'hash2',
        email: 'hash2@ketchum.com',
        password: '123456',
    };
    // Create a persistent agent
    const agent = request.agent(app);
    // Make a POST request to login the user
    let response = await agent
        .post('/auth/login')
        .send(mockUser);
    // Make a POST request to logout the user
    response = await agent
        .post('/auth/logout')
        .send(mockUser);
    // Assert
    // Expect that the response status is 200 (ok)
    expect(response.status).toBe(200);
});

test('refresh token', async () => {
    // Create a mock user object
    const mockUser = {
        username: 'hash2',
        email: 'hash2@ketchum.com',
        password: '123456',
    };
    // Create a persistent agent
    const agent = request.agent(app);
    // Make a POST request to login the user
    let response = await agent
        .post('/auth/login')
        .send(mockUser);
    response = await agent
        .post('/auth/refresh')
        .send(mockUser);
    // Assert
    // Expect that the response status is 200 (ok)
    expect(response.status).toBe(200);
});

test('reject an empty username', async () => {
    // Create a mock user object with an empty username
    const mockUser = {
        username: '',
        email: 'hash2@ketchum.com',
        password: '123456',
    };
    // Make a POST request to create a new user
    const response = await request(app)
        .post('/auth/register')
        .send(mockUser);
    // Assert
    // Expect that the response status is 400 (bad request)
    expect(response.status).toBe(400);
    // Expect that the response body contains the error message
    expect(response.body.errors[0].msg).toBe('Username cannot be empty');
});

test('reject an invalid email', async () => {
    // Create a mock user object with an invalid email
    const mockUser = {
        username: 'hash2',
        email: 'hash2@ketchum',
        password: '123456',
    };
    // Make a POST request to create a new user
    const response = await request(app)
        .post('/auth/register')
        .send(mockUser);
    // Assert
    // Expect that the response status is 400 (bad request)
    expect(response.status).toBe(400);
    // Expect that the response body contains the error message
    expect(response.body.errors[0].msg).toBe('Invalid email address');
});

test('reject a short password', async () => {
    // Create a mock user object with a short password
    const mockUser = {
        username: 'hash2',
        email: 'hash2@ketchum.com',
        password: '123',
    };
    // Make a POST request to create a new user
    const response = await request(app)
        .post('/auth/register')
        .send(mockUser);
    // Assert
    // Expect that the response status is 400 (bad request)
    expect(response.status).toBe(400);
    // Expect that the response body contains the error message
    expect(response.body.errors[0].msg).toBe('Password must be at least 6 characters long');
});

test('reject an empty email', async () => {
    // Create a mock user object with an empty email
    const mockUser = {
        email: '',
        password: '123456',
    };
    // Make a POST request to log in a user
    const response = await request(app)
        .post('/auth/login')
        .send(mockUser);
    // Assert
    // Expect that the response status is 400 (bad request)
    expect(response.status).toBe(400);
    // Expect that the response body contains the error message
    expect(response.body.errors[0].msg).toBe('Email cannot be empty');
});

test('user cant log out if not logged in', async () => {
    // Create a persistent agent
    const agent = request.agent(app);
    // Make a POST request to logout the user
    response = await agent
        .post('/auth/logout')
        
    // Expect that the response status is 200 (ok)
    expect(response.status).toBe(402);
    expect(response.text).toBe("{\"message\":\"user not logged in\"}");
});

//url tests