const Url = require('../models/Url');
const createError = require('http-errors');

class urlRepository {
    async create(url, shortUrl, author) {
        const result = await Url.create({
            "trueUrl": url,
            shortUrl,
            author,
        });
        return result;
    }

    async getByShortUrl(shortUrl) {
        // find the url by shortUrl and increment the counter field
        const url = await Url.findOneAndUpdate(
          { shortUrl }, // filter by shortUrl
          { $inc: { counter: 1 } }, // increment counter by 1
          { new: true } // return the modified document
        );
        return url;
      }
      

    async findByShortUrl(shortUrl) {
      const url = await Url.findOne(
        { shortUrl }, // filter by shortUrl
      );
      return url;
    }

    
    async findByShortUrlId(shortUrlId) {
      const url = await Url.findById(shortUrlId);
      return url;
    }
    

    async destroy(shortUrlId) {
        return Url.findOneAndDelete({ _id: shortUrlId });
    }

    async update(shortUrl, url, shortUrlId) {
        // Update the Url document by short url id
        const result = await Url.findOneAndUpdate(
          { _id: shortUrlId }, // filter by id
          { $set: { shortUrl: shortUrl, trueUrl: url } }, // update fields
          { returnDocument: 'after' } // return the modified document
        );
        return result;
      }
      
      

    async findById(id){
        const result = await Url.findById(id);
        return result;
    }



}

module.exports = new urlRepository();