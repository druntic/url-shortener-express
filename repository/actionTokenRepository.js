const actionToken = require('../models/ActionToken');
const createError = require('http-errors');

class actionTokenRepository{
    async create(id){
        const result = await actionToken.create({
            entityId: id,
            actionName: 'registration_verification',
        });
        return result;
    }

    async findById(id){
        const result = await actionToken.findById(id);
        return result;
    }

    async setAction(accessToken,action){
        accessToken.actionName = action;
        const result = await accessToken.save();
    }
}

module.exports = new actionTokenRepository();