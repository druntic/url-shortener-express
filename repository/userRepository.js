const User = require('../models/User');
const Url = require('../models/Url');
class userRepository {
    async findByEmail(email) {
        const user = await User.findOne({ email: email });
        return user;
    }

    async findByUsername(username) {
        const user = await User.findOne({ username: username });
        return user;
    }
    async findById(id) {
        const user = await User.findById(id);
        return user;
    }

    async findByRefreshToken(refreshToken) {
        const user = await User.findOne({ refreshToken: refreshToken });
        return user;
    }
    async update(user, refreshToken) {
        // find the user by id and update the refreshToken field
        const result = await User.updateOne(
            { _id: user._id }, // filter by id
            { refreshToken: refreshToken } // update field
        );
        return result;
    }

    async create(username, email, hashedPassword) {
        //create and store user
        const result = await User.create({
            username,
            email,
            "password": hashedPassword
        });
        return result;
    }

    // This goes to
    async findUrlsOfUser(user, currentPage, perPage) {
        // Calculate the offset and limit for pagination
        const offset = (currentPage - 1) * perPage;
        const limit = perPage;
        // Find the urls of the user with pagination options
        const urls = await Url.find({ author: user }).skip(offset).limit(limit);
        // Return the urls array
        return urls;
    }

    async verifyEmail(user, time) {
        const result = await User.updateOne(
            { _id: user._id }, // filter by id
            { emailVerifiedAt: time } // update field
        );
        return result;
    }

}
module.exports = new userRepository();