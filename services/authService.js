const userRepository = require("../repository/userRepository");
const tokenRepository = require('../repository/actionTokenRepository');
const createError = require('http-errors');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const actionTokenRepository = require("../repository/actionTokenRepository");
var transport = require('../mail.config');
class authService {
    async store(email, password) {

        const user = await userRepository.findByEmail(email);
        if (!user) throw createError(409, 'user not found');
        if (!user.emailVerifiedAt) throw createError(403, 'email not verified');
        //evaluate password
        const match = await bcrypt.compare(password, user.password);
        if (match) {
            // create JWTs
            const accessToken = jwt.sign(
                { "id": user._id },
                process.env.ACCESS_TOKEN_SECRET,
                { expiresIn: '30s' }
            );
            const refreshToken = jwt.sign(
                { "id": user._id },
                process.env.REFRESH_TOKEN_SECRET,
                { expiresIn: '1d' }
            );
            await userRepository.update(user, refreshToken);
            return { refreshToken, accessToken };
        }
        else {
            throw createError(403, 'password does not match');
        }
    }


    async create(username, email, password) {
        
        const duplicateName = await userRepository.findByUsername(username);
        if (duplicateName) throw createError(409, 'user with that name already exists');
        const duplicateEmail = await userRepository.findByEmail(email);
        if (duplicateEmail) throw createError(409, 'user with that email already exists');

        const hashedPassword = await bcrypt.hash(password, 10);
        const user = await userRepository.create(username, email, hashedPassword);
        const actionToken = await actionTokenRepository.create(user._id.toString());
        // create mail options object
        
        return actionToken._id;

    }

    async refresh(cookies) {

        if (!cookies?.jwt) throw createError(403, 'user not logged in');
        const refreshToken = cookies.jwt;
        const user = await userRepository.findByRefreshToken(refreshToken);
        if (!user) throw createError(403, 'user not found');
        const decoded = await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);
        if (user.id !== decoded.id) throw createError(403, 'username does not match for refresh token');
        const accessToken = jwt.sign(
            { "id": decoded.id },
            process.env.ACCESS_TOKEN_SECRET,
            { expiresIn: '30s' }
        );
        return accessToken;
    }


    async destroy(cookies) {

        if (!cookies?.jwt) throw createError(403, 'user not logged in');
        const refreshToken = cookies.jwt;
        const user = await userRepository.findByRefreshToken(refreshToken);
        if (!user) throw createError(404, 'user not found');
        const decoded = await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);
        if (user.id !== decoded.id) throw createError(403, 'username does not match for refresh token');
        await userRepository.update(user, null);
        return true;
    }

    async verifyEmail(actionTokenId) {

        const actionToken = await actionTokenRepository.findById(actionTokenId);
        if (!actionToken) throw createError(404, 'token not found');
        if (actionToken.actionName == 'registration_verification' && Date.now() < actionToken.expiresAt.getTime()) {
            const user = await userRepository.findById(actionToken.entityId);
            if (!user) throw createError(404, 'user not found');
            actionTokenRepository.setAction(actionToken, 'executed');
            //verify email and create access token for user
            userRepository.verifyEmail(user, Date.now());
            const accessToken = jwt.sign(
                { "id": user._id },
                process.env.ACCESS_TOKEN_SECRET,
                { expiresIn: '30s' }
            );
            const refreshToken = jwt.sign(
                { "id": user._id },
                process.env.REFRESH_TOKEN_SECRET,
                { expiresIn: '1d' }
            );
            // update the user's refresh token in the database
            await userRepository.update(user, refreshToken);
            return { refreshToken, accessToken };
        }
        else {
            throw createError(400, 'link expired or invalid action')
        }
    }

    async sendVerificationEmail(email, id){

        const link = `${process.env.HOST}:${process.env.PORT}/auth/verify-email/${id}`;
        let mailOptions = {
          from: process.env.MAILTRAP_DOMAIN,
          to: email,
          subject: 'You are awesome!',
          text: link
        };
      
        // Return a new promise that resolves or rejects based on the sendMail() callback
        return new Promise((resolve, reject) => {
          transport.sendMail(mailOptions, (error, info) => {
            if (error) {
              reject(error); // Reject the promise with the error
            } else {
              console.log('Email sent: ' + info.response);
              resolve(info); // Resolve the promise with the info
            }
          });
        });
      }

    async resendVerificationEmail(email, callback){
        const user = await userRepository.findByEmail(email);
        if (!user) throw createError(404, 'email not found');
        const id = await actionTokenRepository.create(user._id.toString());
        const link = `${process.env.HOST}:${process.env.PORT}/auth/verify-email/${id}`;
        let mailOptions = {
          from: process.env.MAILTRAP_DOMAIN,
          to: email,
          subject: 'You are awesome!',
          text: link
        };
      
        // Invoke the callback function with the error or info object
        transport.sendMail(mailOptions, (error, info) => {
          if (error) {
            callback(error); // Pass the error to the callback
          } else {
            console.log('Email sent: ' + info.response);
            callback(null, info); // Pass the info to the callback
          }
        });
    }
}




module.exports = new authService();