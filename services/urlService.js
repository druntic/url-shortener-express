const crypto = require('crypto');
const urlRepository = require('../repository/urlRepository');
const createError = require('http-errors');
const userRepository = require('../repository/userRepository');
const { url } = require('inspector');

class urlService {
    async create(url, shortUrl, user) {
        if (!shortUrl) {
            // Create a buffer of 4 random bytes
            const buffer = crypto.randomBytes(4);
            // Convert the buffer to a hexadecimal string
            const hexString = buffer.toString('hex');
            // Return the first 8 characters of the hex string
            shortUrl = hexString.slice(0, 8);
        }
        // Query the urls table using the Url model and the full url
        const urlExists = await urlRepository.findByShortUrl(shortUrl);
        // If the url is not found, throw an error
        if (urlExists) throw createError(409, "Url is already taken");
        
        return urlRepository.create(url, shortUrl, user);
    }

    async get(shortUrlTag) {

        const url = await urlRepository.getByShortUrl(shortUrlTag);
        if (!url) throw createError(404, 'invalid url');
        if (!url.trueUrl.startsWith("http://")) {
            return "http://" + url.trueUrl;
        }
        return url.trueUrl;

    }

    async destroy(shortUrlId, user) {
        //check if logged in user is user that created link
        const url = await urlRepository.findByShortUrlId(shortUrlId);
        if (url.author != user) throw createError(403, "you are forbidden from deleting this url");
        return urlRepository.destroy(shortUrlId);
    }

    async update(shortUrl, url, shortUrlId, user) {
        const urlExists = await urlRepository.findById(shortUrlId);
        if (!urlExists) throw createError(404, 'url with short url id not found');
        const foundUrl = await urlRepository.findByShortUrlId(shortUrlId);
        if (foundUrl.author != user) throw createError(403, "you are forbidden from updating this url");
        return urlRepository.update(shortUrl, url, shortUrlId);
    }

    async show(shortUrlId, user) {
        const url = await urlRepository.findByShortUrlId(shortUrlId);
        if (url.author != user) throw createError(403, "you are forbidden from getting this users url");
        return urlRepository.findById(shortUrlId);
    }
}

module.exports = new urlService();