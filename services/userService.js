const userRepository = require('../repository/userRepository');
const createError = require('http-errors');
class userService{
 async get(user, perPage, currentPage){
    return userRepository.findUrlsOfUser(user, perPage, currentPage);
 }
}
module.exports = new userService();